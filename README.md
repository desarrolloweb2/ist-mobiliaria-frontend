MLC IST

//create database and run all database seeds...
php artisan migrate:fresh --seed

// Refresh the database and run all database seeds...
php artisan migrate:refresh --seed

// Con la opción -mfc estamos indicando que queremos, además de crear un modelo, crear la migración, controlador y model factory.
php artisan make:model Movie -mcf
php artisan make:model ContactAssignment -m

php artisan make:controller PhotoController --resource --model=Photo

php artisan make:migration create_shipments_to_warehouse_has_payments_table --create=shipments_to_warehouse_has_payments

php artisan make:migration products

php artisan make:migration add_timestamps_columns_to_shipment_to_warehouse_has_orders_table --table=shipment_to_warehouse_has_orders

// ejecutar seed de un archivo
php artisan db:seed --class=CategoriesSeeder

# =========================================================

-- Deploy

composer install ( /usr/local/bin/ea-php74 composer.phar i en host de mlc)

php artisan key:generate

# php artisan passport:keys

php artisan migrate:fresh --seed

php artisan storage:link

php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan view:clear

# optimization

composer install --optimize-autoloader --no-dev
php artisan config:cache
php artisan view:cache

cambiar QUEUE_CONNECTION en .env

# modificaciones en el proyecto

# modo mantenimiento (<https://virtumedia.wordpress.com/2019/06/26/publicar-proyecto-laravel/>)

php artisan down

# salir de modo mantenimiento

php artisan up

# para agregar campos en una tabla existente. Ej. (<https://virtumedia.wordpress.com/2019/12/11/modificar-base-de-datos-de-un-sitio-laravel-en-produccion/>)

php artisan make:migration new_fields_users_table --table=users

# =========================================================

---

./vendor/bin/phpunit

cd phpunit-test
php artisan make:test BasicTest

---

# problema de memory limit

COMPOSER_MEMORY_LIMIT=-1 composer require mckenziearts/laravel-notify

COMPOSER_MEMORY_LIMIT=-1 composer require imliam/shareable-link:^1.0.0

=========================================================
Cron Jobs

/usr/local/bin/php /home/username/artisan schedule:run >> /dev/null 2>&1
/usr/local/bin/php /home/username/artisan queue:work --tries=3 --once
=========================================================

# =========================================================

# =========================================================

Errores y soluciones

# ---------------------

file_put_contents(/var/www/app/storage/framework/view
chmod -R 0777 storage/
chmod -R 775 storage/

# ---------------------

# =========================================================

Git

# remove file from cache

git rm --cached

# =========================================================
