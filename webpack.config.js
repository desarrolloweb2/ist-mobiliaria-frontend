const path = require('path')

module.exports = {
    output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
        },
    },
    // add any webpack dev server config here
    // devServer: {
    //     // proxy: {
    //     //     host: 'localhost', // host machine ip
    //     //     port: 8080,
    //     // },
    //     // watchOptions: {
    //     //     aggregateTimeout: 200,
    //     //     poll: 5000,
    //     // },

    //     host: '0.0.0.0',
    //     // port: 8080, // NR
    //     // onBeforeSetupMiddleware(server) {
    //     //     chokidar.watch(['./resources/views/**/*.blade.php']).on('all', function () {
    //     //         server.sockWrite(server.sockets, 'content-changed')
    //     //     })
    //     // },
    // },
}
