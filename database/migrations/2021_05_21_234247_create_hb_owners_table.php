<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateHBOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hb_owners', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('user_id')->index();
            // $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->string('email');
            $table->unsignedBigInteger('hb_id')->comment('id de usuario de hubspot');
            $table->unsignedBigInteger('hb_user_id')->comment('user id de usuario de hubspot');
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hb_owners');
    }
}