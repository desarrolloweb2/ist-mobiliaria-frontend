<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_assignments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hb_owner_id');
            $table->unsignedBigInteger('hb_contact_id');
            $table->string('team');
            // $table->unsignedBigInteger('hb_contact_email');
            $table->timestamps();

            $table->foreign('hb_owner_id')->references('id')->on('hb_owners')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_assignments');
    }
}