<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // \App\Models\User::factory(10)->create();

        // $roleBusinessOwner = Role::create(['name' => 'business_owner']);
        // $permissions = Permission::pluck('id', 'id')->all();

        /**
         * ---------------------------------------------
         */
        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'sadmin@mail.com',
            'password' => bcrypt('123123123'),
        ]);
        //        $user->assignRole([$roleBusinessOwner->id]);

        // $role = Role::create(['name' => 'super-admin']);
        // $role->syncPermissions($permissions);
        // $user->assignRole([$role->id]);

        /**
         * ---------------------------------------------
         */
        // $user = User::create([
        //     'name' => 'Admin',
        //     'email' => 'admin@mail.com',
        //     'password' => bcrypt('123'),
        //     'short_name' => 'admin',
        // ]);

        // BusinessHasUsers::create([
        //     'business_id' => $business->id,
        //     'user_id' => $user->id,
        // ]);

        // $user->assignRole([$roleBusinessOwner->id]);

        // $role = Role::create(['name' => 'admin']);
        // $role->syncPermissions($permissions);
        // $user->assignRole([$role->id]);

        /**
         * ---------------------------------------------
         */

        /**
         * Asesor 1
         */
        $user = User::create([
            'name' => 'Asesor 1',
            'email' => 'asesor1@mail.com',
            'password' => bcrypt('123'),
        ]);

        /**
         * Asesor 2
         */
        $user = User::create([
            'name' => 'Asesor 2',
            'email' => 'asesor2@mail.com',
            'password' => bcrypt('123'),
        ]);

        /**
         * Asesor 3
         */
        $user = User::create([
            'name' => 'Asesor 3',
            'email' => 'asesor3@mail.com',
            'password' => bcrypt('123'),
        ]);

        /**
         * Asesor 4
         */
        $user = User::create([
            'name' => 'Asesor 4',
            'email' => 'asesor4@mail.com',
            'password' => bcrypt('123'),
        ]);

        /**
         * Asesor 5
         */
        $user = User::create([
            'name' => 'Asesor 5',
            'email' => 'asesor5@mail.com',
            'password' => bcrypt('123'),
        ]);

        /**
         * Asesor 6
         */
        $user = User::create([
            'name' => 'Asesor 6',
            'email' => 'asesor6@mail.com',
            'password' => bcrypt('123'),
        ]);

    }
}