<?php

namespace Database\Seeders;

use App\Models\UserGuardias;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserGuardiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $elements = [
            //
            [
                'user_id' => 2,
                'start_at' => '2021-05-24 9:00:00',
                'end_at' => '2021-05-24 14:00:00',
            ],
            [
                'user_id' => 3,
                'start_at' => '2021-05-24 14:00:00',
                'end_at' => '2021-05-24 18:00:00',
            ],
            //
            [
                'user_id' => 2,
                'start_at' => '2021-05-25 9:00:00',
                'end_at' => '2021-05-25 14:00:00',
            ],
            [
                'user_id' => 3,
                'start_at' => '2021-05-25 14:00:00',
                'end_at' => '2021-05-25 18:00:00',
            ],
            [
                'user_id' => 4,
                'start_at' => '2021-05-25 9:00:00',
                'end_at' => '2021-05-25 14:00:00',
            ],
            [
                'user_id' => 5,
                'start_at' => '2021-05-25 9:00:00',
                'end_at' => '2021-05-25 14:00:00',
            ],
            [
                'user_id' => 6,
                'start_at' => '2021-05-25 16:00:00',
                'end_at' => '2021-05-25 20:00:00',
            ],
            [
                'user_id' => 7,
                'start_at' => '2021-05-25 14:00:00',
                'end_at' => '2021-05-25 20:00:00',
            ],
            //
            [
                'user_id' => 2,
                'start_at' => '2021-05-26 9:00:00',
                'end_at' => '2021-05-26 14:00:00',
            ],
            [
                'user_id' => 3,
                'start_at' => '2021-05-26 14:00:00',
                'end_at' => '2021-05-26 18:00:00',
            ],
        ];

        foreach ($elements as $el) {

            $guardia = new UserGuardias();
            $guardia->user_id = $el['user_id'];

            $guardia->start_at = Carbon::createFromFormat('Y-m-d H:i:s', $el['start_at']);
            $guardia->end_at = Carbon::createFromFormat('Y-m-d H:i:s', $el['end_at']);

            $guardia->save();

        }

    }
}