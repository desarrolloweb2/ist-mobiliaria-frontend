<?php

namespace Database\Seeders;

use App\Models\HBOwners;
use Illuminate\Database\Seeder;

class UserHBOwnersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        /**
         * ---------------------------------------------
         */

        /**
         * Asesor 1, Owner 1
         */
        $user = HBOwners::create([
            'user_id' => 2,
            'name' => 'asesor 1',
            'email' => 'asesor1@mail.com',
            'hb_id' => 76323926,
            'hb_user_id' => 24671493,
        ]);

        /**
         * Asesor 2, Owner 2
         */
        $user = HBOwners::create([
            'user_id' => 3,
            'name' => 'asesor 2',
            'email' => 'asesor2@mail.com',
            'hb_id' => 81122705,
            'hb_user_id' => 24888982,
        ]);

        /**
         * Asesor 3, Owner 3
         */
        $user = HBOwners::create([
            'user_id' => 4,
            'name' => 'asesor 3',
            'email' => 'asesor3@mail.com',
            'hb_id' => 81251606,
            'hb_user_id' => 24904122,
        ]);

        /**
         * Asesor 4, Owner 4
         */
        $user = HBOwners::create([
            'user_id' => 5,
            'name' => 'asesor 4',
            'email' => 'asesor4@mail.com',
            'hb_id' => 81251609,
            'hb_user_id' => 24904127,
        ]);

        /**
         * Asesor 5, Owner 5
         */
        $user = HBOwners::create([
            'user_id' => 6,
            'name' => 'asesor 5',
            'email' => 'asesor5@mail.com',
            'hb_id' => 81251627,
            'hb_user_id' => 24904131,
        ]);

        /**
         * Asesor 6, Owner6
         */
        $user = HBOwners::create([
            'user_id' => 7,
            'name' => 'asesor 6',
            'email' => 'asesor6@mail.com',
            'hb_id' => 81251929,
            'hb_user_id' => 24904133,
        ]);

    }
}