<?php

namespace Database\Seeders;

use App\Models\User;
use Exception;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = Permission::pluck('id', 'id')->all();

        /**
         * ---------------------------------------------
         */
        try {
            $user = User::where('email', 'sadmin@mail.com')->first();

            $role = Role::create(['name' => 'super-admin']);
            $role->syncPermissions($permissions);

            $user->assignRole([$role->id]);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
        /**
         * ---------------------------------------------
         */

        try {
            $role = Role::create(['name' => 'asesor']);
            $role->syncPermissions($permissions);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        $usersHBOwners = User::query()
            ->rightJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
            ->select('hb_owners.*')
            ->get();

        echo "Asesores: " . $usersHBOwners->count();

        foreach ($usersHBOwners as $userHBOwner) {

            try {
                $user = $userHBOwner->user;

                if ($user) {
                    echo "\nname: " . $user->name;
                    $user->assignRole([$role->id]);
                } else {
                    echo "\nNo user. " . $userHBOwner->email;
                }

            } catch (Exception $e) {
                var_dump($e->getMessage());
            }
        }

        /**
         * ---------------------------------------------
         */

    }
}