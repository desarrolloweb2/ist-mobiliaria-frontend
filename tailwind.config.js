const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            //
            // PRUEBA
            borderColor: (theme) => ({
                DEFAULT: theme('colors.gray.200', 'currentColor'),
            }),
            //   fontFamily: {
            //     sans: ['Cerebri Sans', ...defaultTheme.fontFamily.sans],
            //   },
            boxShadow: (theme) => ({
                outline: '0 0 0 2px ' + theme('colors.indigo.500'),
            }),
            fill: (theme) => theme('colors'),
            //
            colors: {
                orange: colors.orange,
            },
            // END PRUEBA
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
}
