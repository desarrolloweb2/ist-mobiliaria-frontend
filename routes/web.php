<?php

use App\Http\Controllers\AsesoresController;
use App\Http\Controllers\AsesoresParaisoController;
use App\Http\Controllers\ContactosController;
use App\Http\Controllers\GuardiasController;
use App\Http\Controllers\GuardiasParaisoController;
use App\Models\HBOwners;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {

	return redirect()->route('login');

	return Inertia::render('Home', [
		'canLogin' => Route::has('login'),
		// 'canRegister' => Route::has('register'),
		// 'laravelVersion' => Application::VERSION,
		// 'phpVersion' => PHP_VERSION,
	]);
});

// =====================
// SECURE ROUTES
// =====================
//
Route::middleware(['auth:sanctum']) // , 'verified'
	->get('/dashboard', function () {

		$asesores_count = HBOwners::count();

		// Request::validate([
		//     'email' => ['required', 'max:50', 'email'],
		// ]);

		// session()->flash('flash.banner', 'Yay for free components!');
		// session()->flash('flash.bannerStyle', 'success');

		// Session::flash('flash.banner', 'Realizado');
		// Session::flash('flash.bannerStyle', 'Realizado');

		// session()->flash('message', 'Mensaje flash ');
		// session()->flash('success', 'Mensaje flash success');

		// session()->flash('toast', ['type' => 'success', 'message' => 'Realizado toast regular']);

		// Session::flash('success', 'Realizado');

		return Inertia::render('Dashboard', ['asesores_count' => $asesores_count])
			// ->with('message', 'Mensaje en dashboard.')
			// ->with(['toast' => ['type' => 'success', 'message' => 'Realizado with']])
			// ->withFlash('the post is saved!')
		;
	})->name('dashboard');

//Asesores 1 
Route::group(['middleware' => 'auth:sanctum'], function () {

	// asesores (propietatios / hb_owners )
	Route::group(['prefix' => 'asesores'], function () {
		Route::get('/', [AsesoresController::class, 'index'])->name('asesores.index');
		Route::delete('/{HBOwners}', [AsesoresController::class, 'destroy'])->name('asesores.destroy');
	});
	// contactos (leads)
	Route::group(['prefix' => 'contactos'], function () {
		Route::get('/', [ContactosController::class, 'index'])->name('contactos.index');
	});

	// Guardias
	Route::group(['prefix' => 'guardias'], function () {

		Route::post('/from-file', [GuardiasController::class, 'storeFromFile']);

		Route::get('/index', [GuardiasController::class, 'index'])->name('guardias.index');

		Route::get('/{date?}', [GuardiasController::class, 'dashboard'])
			->name('guardias.dashboard');

		Route::delete('/{UserGuardias}', [GuardiasController::class, 'destroy'])
			->where('date', '[0-9]+')
			->name('guardias.destroy');
	});
});

//Asesores Paraíso
Route::group(['middleware' => 'auth:sanctum'], function () {

	// asesores Paraíso (propietatios / hb_owners )
	Route::group(['prefix' => 'asesoresParaiso'], function () {
		Route::get('/', [AsesoresParaisoController::class, 'index'])->name('asesoresParaiso.index');
		Route::delete('/{HBOwners}', [AsesoresParaisoController::class, 'destroy'])->name('asesoresParaiso.destroy');
	});

	// Guardias Paraaíso
	Route::group(['prefix' => 'guardiasParaiso'], function () {

		Route::post('/from-file', [GuardiasParaisoController::class, 'storeFromFile']);

		Route::get('/index', [GuardiasParaisoController::class, 'index'])->name('guardiasParaiso.index');

		Route::get('/{date?}', [GuardiasParaisoController::class, 'dashboard'])->name('guardiasParaiso.dashboard');

		Route::delete('/{userParaisoGuardia}', [GuardiasParaisoController::class, 'destroy'])
			->where('date', '[0-9]+')
			->name('guardiasParaiso.destroy');
	});
});
