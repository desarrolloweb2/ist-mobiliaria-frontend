<?php

use App\Http\Controllers\APIEndpointsForHB;
use App\Http\Controllers\AsesoresController;
use App\Http\Controllers\AsesoresParaisoController;
use App\Http\Controllers\GuardiasController;
use App\Http\Controllers\GuardiasParaisoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
	return $request->user();
});

Route::get('/', function () {
	Log::info('api');
	return ['done' => 'succes'];
});

// =====================
// SECURE ROUTES
// =====================
//

//
Route::group([
	// 'middleware' => 'auth:sanctum'
	// 'middleware' => 'auth',
], function () {

	// lead-rotation
	Route::post('/lead-rotation', [APIEndpointsForHB::class, 'leadRotation']);

	// wf-reinscription
	Route::post('/wf-reinscription', [APIEndpointsForHB::class, 'wfReinscription']);

	// Guardias
	// Route::get('/asesores/guardias', [GuardiasController::class, 'guardiasDashoardAPI']);
	Route::group(['prefix' => '/guardias'], function () {
		Route::get('/', [GuardiasController::class, 'index_api']);
		Route::post('/add', [GuardiasController::class, 'store']);

		Route::delete('/{userGuardia}', [GuardiasController::class, 'destroy']);
	});

	// Guardias Paraíso
	Route::group(['prefix' => '/guardiasParaiso'], function () {
		Route::get('/', [GuardiasParaisoController::class, 'index_api']);
		Route::post('/add', [GuardiasParaisoController::class, 'store']);

		Route::delete('/{userParaisoGuardia}', [GuardiasParaisoController::class, 'destroy']);
	});


	// Asesores
	Route::group(['prefix' => '/asesores'], function () {

		// se usa para el listado en guardias
		Route::get('/', [AsesoresController::class, 'index_api']);

		Route::get('/hb-update', [AsesoresController::class, 'HBUpdateOwnersAPI']);
	});

	// Asesores Paraiso
	Route::group(['prefix' => '/asesoresParaiso'], function () {

		// se usa para el listado en guardias
		Route::get('/', [AsesoresParaisoController::class, 'index_api']);
		Route::get('/hb-update', [AsesoresParaisoController::class, 'HBUpdateOwnersAPI']);
	});
});
