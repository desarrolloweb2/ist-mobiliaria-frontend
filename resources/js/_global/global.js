import { reactive, readonly } from 'vue'

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    },
})

const state = reactive({
    count: 0,
})

const menus = reactive({
    menuItems: [{
            routeName: 'dashboard',
            title: 'Dashboard',
            icon: 'dashboard',
        },
        {
            routeName: 'asesores.index',
            title: 'Asesores Villahermosa',
            icon: 'users',
        },
        {
            routeName: 'asesoresParaiso.index',
            title: 'Asesores Paraíso',
            icon: 'users',
        },
        // {
        //     routeName: 'guardias.index',
        //     title: 'Guardias por día',
        //     icon: 'clock',
        // },
        {
            routeName: 'guardias.dashboard',
            title: 'Guardias Villahermosa',
            icon: 'calendar',
        },
        {
            routeName: 'guardiasParaiso.dashboard',
            title: 'Guardias Paraíso',
            icon: 'calendar',
        },
        // {
        //     routeName: 'contactos.index',
        //     title: 'Contactos',
        //     icon: 'users_group',
        // },
    ],
})

const increment = function() {
    state.count++
}

export default {
    Toast,
    state: readonly(state),
    increment,
    menuItems: readonly(menus.menuItems),
}