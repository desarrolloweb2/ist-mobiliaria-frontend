import { onBeforeUnmount, ref } from 'vue'

export const fetchDataInterval = (props) => {
    let server_datetime = ref(props.server_datetime)
    let asesores_disponibles_hoy = ref(props.asesores_disponibles_hoy)
    let asesores_disponibles_en_este_momento = ref(props.asesores_disponibles_en_este_momento)

    const params = new URLSearchParams(window.location.search)
    // console.log('params', params)
    // for (const param of params) {
    //     console.log(param)
    // }

    let paramsStr = params

    let getData = () => {
        axios.get('/api/guardias?' + paramsStr).then((response) => {
            let responseData = response.data

            server_datetime.value = responseData.server_datetime
            asesores_disponibles_hoy.value = responseData.asesores_disponibles_hoy
            asesores_disponibles_en_este_momento.value = responseData.asesores_disponibles_en_este_momento
        })
    }

    const intervalHandle = setInterval(getData, 20000)

    onBeforeUnmount(() => clearInterval(intervalHandle.value))

    return {
        server_datetime,
        asesores_disponibles_hoy,
        asesores_disponibles_en_este_momento,
    }
}
