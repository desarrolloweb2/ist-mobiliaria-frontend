// Importamos Vue
import Vue from 'vue'

// Importamos momentjs (instalado con npm install moment --save)
import moment from 'moment'

// Fecha
Vue.filter('date', (date, format = 'DD MMM YYYY', locate = 'es') => {
    if (date) {
        // TODO: Tomar de la sesion si existen sino poner defaults
        // var format=format;
        // var locate=locate;

        // Construimos fecha
        moment.locale(locate)
        var dateFormated = moment(new String(date), moment.ISO_8601).format(format)
        return dateFormated
    }
})

// Hora
Vue.filter('time', (date, format = 'h:mm:ss A', locate = 'es') => {
    if (date) {
        // TODO: Tomar de la sesion si existen sino poner defaults
        // var format=format;
        // var locate=locate;

        // Construimos fecha
        moment.locale(locate)
        var dateFormated = moment(new String(date), moment.ISO_8601).format(format)
        return dateFormated
    }
})
