import { defineComponent, ref } from 'vue'

// export default defineComponent({
//     name: 'datetimeFilter',
//     setup() {
//         console.log('datetimeFilter setup ')
//         const data = ref(null)
//         data.value = 'aaaaaa'

//         let datetimeFilter = 'datetimeFilter valor'

//         return { data, datetimeFilter }
//     },
//     // template: `<pre>{{ data }}</pre>`,
// })

export const dateFilter = (date, format = 'DD MMM YYYY', locate = 'es') => {
    let dateFormated = null

    if (date) {
        // TODO: Tomar de la sesion si existen sino poner defaults
        // var format=format;
        // var locate=locate;

        // Construimos fecha
        moment.locale(locate)
        dateFormated = moment(new String(date), moment.ISO_8601).format(format)
    }

    return { dateFormated }
}

export const timeFilter = (date, format = 'DD MMM YYYY', locate = 'es') => {
    let dateFormated = null

    if (date) {
        // TODO: Tomar de la sesion si existen sino poner defaults
        // var format=format;
        // var locate=locate;

        // Construimos fecha
        moment.locale(locate)
        dateFormated = moment(new String(date), moment.ISO_8601).format(format)
    }

    return { dateFormated }
}
