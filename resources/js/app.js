require('./bootstrap')

// Import modules...
import { createApp, h, version } from 'vue'
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3'
import { InertiaProgress } from '@inertiajs/progress'

InertiaProgress.init({ color: '#4B5563' })

// https://vcalendar.io/examples/datepickers.html#button-dropdown
import { SetupCalendar, Calendar, DatePicker } from 'v-calendar'

import global from '@/_global/global'

// import Flash from '@/components/Flash.vue'

const el = document.getElementById('app')

createApp({
    // metaInfo: {
    //     titleTemplate: (title) => (title ? `${title} - System` : 'System'),
    // },
    data: () => ({
        // vueVersion: version,
    }),
    provide: {
        global,
    },
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
})
    .mixin({ methods: { route } })
    .use(InertiaPlugin)
    // Setup the plugin with optional defaults
    .use(SetupCalendar, {})
    // Use the components
    .component('Calendar', Calendar)
    .component('DatePicker', DatePicker)
    .mount(el)
