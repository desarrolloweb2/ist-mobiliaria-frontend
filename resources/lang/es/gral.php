<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General
    |--------------------------------------------------------------------------
    |
    | The following language lines are used generally
    |
    */

    'Profile' => 'Perfil',
    'Settings' => 'Configuración'
];
