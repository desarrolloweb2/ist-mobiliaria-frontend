@if (env('APP_ENV') == 'local')
    <!-- Active Breakpoint Indicator -->
    <div
        class="fixed top-0 left-0 flex items-center justify-center w-6 h-6 p-3 m-8 text-xs text-white bg-gray-700 rounded-full Aabsolute sm:bg-pink-500 md:bg-orange-500 lg:bg-green-500 xl:bg-blue-500 2xl:bg-red-500"
        style="z-index: 100">
        <div class="block sm:hidden md:hidden lg:hidden xl:hidden 2xl:hidden">xs</div>
        <div class="hidden sm:block md:hidden lg:hidden xl:hidden 2xl:hidden">sm</div>
        <div class="hidden sm:hidden md:block lg:hidden xl:hidden 2xl:hidden">md</div>
        <div class="hidden sm:hidden md:hidden lg:block xl:hidden 2xl:hidden">lg</div>
        <div class="hidden sm:hidden md:hidden lg:hidden xl:block 2xl:hidden ">xl</div>
        <div class="hidden sm:hidden md:hidden lg:hidden xl:hidden 2xl:block">2xl</div>
    </div>
    <!-- /Active Breakpoint Indicator -->
@endif
