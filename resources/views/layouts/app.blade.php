<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>


        <script defer src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" >

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app2.css') }}">

        <!-- Scripts -->
        <script src="{{ mix('js/app2.js') }}" defer></script>


        @routes
    </head>

    <body class="font-sans antialiased layout.app ">

        @include('partial/breakpoint')

        @yield('content')

        {{-- {{ app()->getLocale()}} --}}
        @inertia

    </body>
</html>
