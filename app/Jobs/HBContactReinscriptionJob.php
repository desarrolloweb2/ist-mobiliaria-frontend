<?php

namespace App\Jobs;

use App\Models\ContactToReinscription;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class HBContactReinscriptionJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $contactArr;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($contactArr)
	{

		$this->contactArr = $contactArr;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$contactId = (int) $this->contactArr['hb_contact_id'];

		$hubSpotBaseUrl = config('hubspot.base_url');
		$hapikey = config('hubspot.hapy_key');
		$email = null;

		if (isset($this->contactArr['email']) && !empty($this->contactArr['email'])) {
			$email = $this->contactArr['email'];
		} else {
			// buscamos los datos del contacto, para obtener el email
			try {

				$resourceUrl = '/crm/v3/objects/contacts/' . $contactId;
				$response = Http::get($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey);

				if ($response->status() !== 200) {
					Log::warning(' ------------------------------------ ');
					Log::warning(' error: hb call');
					Log::warning(print_r($response->status(), true));
					Log::warning(print_r($response->body(), true));
					Log::warning(' ------------------------------------ ');
				} else {
					$bodyJson = $response->json();
					$email = $bodyJson['properties']['email'];
				}
			} catch (Exception $e) {
				Log::warning(' ------------------------------------ ');
				Log::warning(' error ');
				Log::warning(print_r($e->getCode(), true));
				Log::warning(print_r($e->getMessage(), true));
				Log::warning(' ------------------------------------ ');
			}
		}

		if ($email) {

			/**
			 * reasignar contacto a workflow
			 * {{hubSpotBaseUrl}}/crm/v3/objects/contacts/:contactid?hapikey={{key cta d pruebas}}
			 *
			 */

			try {

				if ($this->contactArr['hb_contact_team'] == config('hubspot.asesores_paraiso_teamname')) {
					$workflowId = config('hubspot.workflow_paraiso_id'); //31973178; //Establecer etapa del ciclo de vida en MQL        
				} else {
					$workflowId = config('hubspot.workflow_villa_hermosa_id'); //25186489; //Establecer etapa del ciclo de vida en MQL       
				}
				//$workflowId = config('hubspot.workflow_id'); //25186489; //Establecer etapa del ciclo de vida en MQL
				$resourceUrl = '/automation/v2/workflows/' . $workflowId . '/enrollments/contacts/' . $email;

				$response = Http::post($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey, []);

				if ($response->status() !== 204) {
					Log::warning(' ------------------------------------ ');
					Log::warning('Error al reinscribir contacto (#' . $contactId . ' - ' . $email . ')');
					Log::warning(print_r($response->status(), true));
					Log::warning(print_r($response->body(), true));
					Log::warning(' ------------------------------------ ');

					$this->eliminarContactoDeWorkflow($contactId, $email, $this->contactArr['team']);
				} else {

					Log::info(' ------------------------------------ ');
					Log::info(print_r($response->body(), true));
					// Log::info($response->body(), true);
					Log::info(' contacto (#' . $contactId . ' - ' . $email . ') reinscrito al workflow (' . $workflowId . ')');
					Log::info(' ------------------------------------ ');
				}
			} catch (Exception $e) {
				Log::warning(' ------------------------------------ ');
				Log::warning(' error ');
				Log::warning(print_r($e->getCode(), true));
				Log::warning(print_r($e->getMessage(), true));
				Log::warning(' ------------------------------------ ');
			}

			// lo eliminamos de la tabla, si existe (aunq haya salido mal, el job ContactsNewNotAssignedReinscriptionCmd lo inscribira mas tarde)
			ContactToReinscription::query()->where('hb_contact_id', '=', $contactId)->delete();
		}
	}

	/**
	 *
	 */
	private function eliminarContactoDeWorkflow($contactId, $email, $team)
	{

		/**
		 * Eliminar contactos del workflow
		 * {{hubSpotBaseUrl}}/automation/v2/workflows?hapikey={{key cta d pruebas}}
		 *
		 */

		$hubSpotBaseUrl = config('hubspot.base_url');
		$hapikey = config('hubspot.hapy_key'); //'6a4d3deb-96d4-4124-ae79-b468f3698314'; // key cta d pruebas

		try {
			if ($team  == config('hubspot.asesores_paraiso_teamname')) {
				$workflowId = config('hubspot.workflow_paraiso_id'); //31973178; //Establecer etapa del ciclo de vida en MQL
			} else {
				$workflowId = config('hubspot.workflow_villa_hermosa_id'); //25186489; //Establecer etapa del ciclo de vida en MQL
			}
			//$workflowId = config('hubspot.workflow_id'); //25186489; //Establecer etapa del ciclo de vida en MQL
			$resourceUrl = '/automation/v2/workflows/' . $workflowId . '/enrollments/contacts/' . $email;

			$response = Http::delete($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey, []);

			if ($response->status() !== 204) {
				Log::warning(' ------------------------------------ ');
				Log::warning('Error al eliminar contacto (#' . $contactId . ' - ' . $email . '), del workflow');
				Log::warning(print_r($response->status(), true));
				Log::warning(print_r($response->body(), true));
				Log::warning(' ------------------------------------ ');
			} else {

				Log::info(' ------------------------------------ ');
				Log::info(print_r($response->body(), true));
				// Log::info($response->body(), true);
				Log::info(' contacto (#' . $contactId . ' - ' . $email . ') eliminado de workflow (' . $workflowId . ')');
				Log::info(' ------------------------------------ ');
			}
		} catch (Exception $e) {
			Log::warning(' ------------------------------------ ');
			Log::warning(' error ');
			Log::warning(print_r($e->getCode(), true));
			Log::warning(print_r($e->getMessage(), true));
			Log::warning(' ------------------------------------ ');
		}
	}
}
