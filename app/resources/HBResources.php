<?php

namespace App\resources;

use App\Models\HBContacts;
use App\Models\HBOwners;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class HBResources
{

	//
	public static function UpdateHBOwners($typeGroup)
	{
		$result = ['data' => []];

		/**
		 * Obtener los owners (propietarios, AKA: asesores)
		 * {{hubSpotBaseUrl}}/crm/v3/owners/?hapikey={{key cta d pruebas}}
		 *
		 */

		$hubSpotBaseUrl = config('hubspot.base_url');
		$hapikey = config('hubspot.hapy_key'); //'6a4d3deb-96d4-4124-ae79-b468f3698314'; // key cta d pruebas
		$resourceUrl = '/crm/v3/owners/';
		if ($typeGroup == config('hubspot.asesores_paraiso_teamname')) {
			$setTeam = config('hubspot.asesores_paraiso_teamname');
		} else {
			$setTeam = config('hubspot.asesores_teamname');
		}

		$asesores_teamname = $setTeam;

		try {

			$response = Http::get($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey);

			if ($response->status() !== 200) {
				Log::warning(' ------------------------------------ ');
				Log::warning(' error: hb call');
				Log::warning(print_r($response->status(), true));
				Log::warning(print_r($response->body(), true));
				Log::warning(' ------------------------------------ ');

				if (App::environment() != 'production') {
					//return response($response->body(), $response->status());
					//dd($result);
				}
			} else {

				foreach ($response->json()['results'] as $el) {
					if (isset($el['teams'])) {
						$ownerValid = false;
						foreach ($el['teams'] as $teamArr) {

							if ($teamArr['name'] == $asesores_teamname) {

								//   if ($teamArr['name'] == 'Ventas paraíso') {
								$ownerValid = true;
								$result['data']['valid'][] = $el;

								continue;
							}
						}

						if ($ownerValid) {

							try {
								$attributes = ['email' =>  $el['email'], 'name' => $el['firstName'] . ' ' . $el['lastName'], 'password' => Hash::make(Str::random(8))];
								//Busca los registros que esten igualmente eliminados  "deleted_at"
								$user = User::query()->where('email', $el['email'])->withTrashed()->first() ?: User::firstOrCreate($attributes);

								if ($el['firstName'] != "" && $el['lastName'] != "") {

									$el['firstName'];
									$completeName = $el['firstName'] . ' ' . $el['lastName'];
								} else {

									$completeName = "NULL";
								}

								if (!HBOwners::query()->where('email', $el['email'])->withTrashed()->first()) {
									$hbOwner = new HBOwners();
									$hbOwner->user_id = $user->id;
									$hbOwner->name = $completeName;
									$hbOwner->email = $el['email'];
									$hbOwner->hb_id =  $el['id'];
									$hbOwner->hb_user_id = $el['userId'];
									$hbOwner->meta = isset($el['teams']) ? serialize($el['teams']) : '';
									$hbOwner->save();

									$result['data']['saved'][] = $hbOwner->toArray();
								}
							} catch (Exception $e) {
								Log::warning('Error saving');
								Log::warning($e->getMessage());
								echo $e->getMessage();
							}
						}
					}
				}
			}
		} catch (Exception $e) {
			Log::warning(' ------------------------------------ ');
			Log::warning(' error ');
			Log::warning(print_r($e->getCode(), true));
			Log::warning(print_r($e->getMessage(), true));
			Log::warning(' ------------------------------------ ');
		}

		return $result;
	}

	// crea un hb_contact si no existe
	public static function createHBContactIfNotExists($contactId)
	{

		$result = ['data' => []];

		if ($hbContact = HBContacts::where('hb_id', $contactId)->first()) {
			$result['data'] = $hbContact->toArray();

			$result['info'] = 'user exists';
		} else {
			/**
			 * traemos datos del contacto
			 * {{hubSpotBaseUrl}}/crm/v3/objects/contacts/:contactid?hapikey={{key}}
			 *
			 */

			$hubSpotBaseUrl = config('hubspot.base_url');
			$hapikey = config('hubspot.hapy_key'); //'6a4d3deb-96d4-4124-ae79-b468f3698314'; // key cta d pruebas
			$resourceUrl = '/crm/v3/objects/contacts/' . $contactId;

			try {

				$response = Http::get($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey);

				if ($response->status() !== 200) {
					Log::warning(' ------------------------------------ ');
					Log::warning(' error: hb call');
					Log::warning(print_r($response->status(), true));
					Log::warning(print_r($response->body(), true));
					Log::warning(' ------------------------------------ ');
				} else {

					$bodyJson = $response->json();

					$el = $bodyJson['properties'];

					$user = User::firstOrCreate(
						['email' => $el['email']],
						[
							'name' => $el['firstname'] . ' ' . $el['lastname'],
							'password' => Hash::make(Str::random(8)),
						]
					);

					if (!HBContacts::query()->where('email', $el['email'])->first()) {
						$hbContact = new HBContacts();
						$hbContact->user_id = $user->id;
						$hbContact->name = $el['firstname'] . ' ' . $el['lastname'];
						$hbContact->email = $el['email'];
						$hbContact->hb_id = $bodyJson['id'];
						$hbContact->hb_user_id = $bodyJson['id'];
						$hbContact->meta = serialize($bodyJson);
						$hbContact->save();

						$result['data'] = $hbContact->toArray();
					}
				}
			} catch (Exception $e) {
				Log::warning(' ------------------------------------ ');
				Log::warning(' error ');
				Log::warning(print_r($e->getCode(), true));
				Log::warning(print_r($e->getMessage(), true));
				Log::warning(' ------------------------------------ ');
			}
		}

		return $result;
	}
}
