<?php

namespace App\Console\Commands;

use App\Jobs\HBContactReinscriptionJob;
use App\Models\ContactToReinscription;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ContactReinscriptionCmd extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'hb:contact_reinscription';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reinscribe al workflow, a los contactos que tengan mas de 5 min en la tabla "contact_to_reinscriptions"';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{

		Log::info(' ------------------------------------ ');
		Log::info(' ContactReinscriptionCmd');
		Log::info(' ------------------------------------ ');

		$date = new DateTime();
		$date->modify('-3 minutes');
		$formatted_date = $date->format('Y-m-d H:i:s');
		// dump($formatted_date);
		$team = 'Rotación de guardias villahermosa';
		$contacts = ContactToReinscription::query()
			->where('created_at', '<=', $formatted_date)
			->where('team', '=', $team)
			->get();

		echo "\nHay " . $contacts->count() . " registros";
		Log::info("Hay " . $contacts->count() . " registros");

		foreach ($contacts as $contact) {

			Log::info(' ------------------------------------ ');
			Log::info(' Creando job para reinscribir al contacto ' . $contact->hb_contact_id);
			Log::info(' ------------------------------------ ');

			$contactArr = [
				'hb_contact_id' => $contact->hb_contact_id,
				'hb_contact_team' => $team
			];

			HBContactReinscriptionJob::dispatch($contactArr);
			// dispatch(new HBContactReinscriptionJob($contactArr));

			// lo eliminamos de la tabla
			$contact->delete();
		}

		// return 0;
	}
}
