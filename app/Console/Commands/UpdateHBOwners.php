<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class UpdateHBOwners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hb:updateHBOwners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza en el sistema, los owners (asesores) que hay en HubSpot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Log::info(' ------------------------------------ ');
        Log::info(' UpdateHBOwners');
        Log::info(' ------------------------------------ ');

        return 1;
    }
}