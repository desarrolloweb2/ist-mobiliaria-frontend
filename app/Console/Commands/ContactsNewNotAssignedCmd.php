<?php

namespace App\Console\Commands;

use App\Models\ContactToReinscription;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ContactsNewNotAssignedCmd extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'hb:contacts_new_not_assigned_reinscription';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Busca por API, contactos que no se hayan inscrito al workflow, despues de cierto tiempo';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		// return 0;

		Log::info(' ------------------------------------ ');
		Log::info(' ContactsNewNotAssignedCmd ');
		Log::info(' ------------------------------------ ');

		/**
		 * Buscar contactos nuevos, no asignados
		 * {{hubSpotBaseUrl}}/crm/v3/objects/contacts/search?hapikey={{key cta d pruebas}}
		 *
		 */

		$hubSpotBaseUrl = config('hubspot.base_url');
		$hapikey = config('hubspot.hapy_key'); //'6a4d3deb-96d4-4124-ae79-b468f3698314'; // key cta d pruebas
		$resourceUrl = '/crm/v3/objects/contacts/search';
		$team = "Rotación de guardias villahermosa";

		try {

			$bodyReq = [
				"filterGroups" => [
					[
						"filters" => [
							[
								"value" => null,
								"propertyName" => "hubspot_owner_id",
								"operator" => "NOT_HAS_PROPERTY",
							],
							[
								"value" => "NEW",
								"propertyName" => "hs_lead_status",
								"operator" => "EQ",
							]
						],
					],
				],
			];

			$response = Http::post($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey, $bodyReq);

			if ($response->status() !== 200) {
				Log::warning(' ------------------------------------ ');
				Log::warning(' error: hb call');
				Log::warning(print_r($response->status(), true));
				Log::warning(print_r($response->body(), true));
				Log::warning(' ------------------------------------ ');
			} else {

				$bodyJson = $response->json();

				foreach ($bodyJson['results'] as $el) {

					// TODO: asegurarse de que el contacto no este en el workflow para evitar este error
					// [2021-05-26 17:03:01] local.WARNING: 412
					// [2021-05-26 17:03:01] local.WARNING: {"status":"error","message":"This contact is already an active member of the workflow","correlationId":"e31ff644-4d64-406a-9559-926c21ed7297","type":"ALREADY_ACTIVE_IN_WORKFLOW"}

					// $this->eliminarContactoDeWorkflow($el['id'], $el['properties']['email']);

					// ContactToReinscription::updateOrCreate(
					//     ['hb_contact_id' => $el['id']],
					//     ['hb_contact_id' => $el['id']]
					// );
					$contactToReinscirptionNext = new ContactToReinscription();
					$contactToReinscirptionNext->hb_contact_id = $el['id'];
					$contactToReinscirptionNext->team = $team;
					$contactToReinscirptionNext->save();

					Log::info(' ------------------------------------ ');
					Log::info(' Contacto ' . $el['id'] . ' guardado para reinscribir en workflow - ContactsNewNotAssignedCmd');
					Log::info(' ------------------------------------ ');
				}
			}
		} catch (Exception $e) {
			Log::warning(' ------------------------------------ ');
			Log::warning(' error ');
			Log::warning(print_r($e->getCode(), true));
			Log::warning(print_r($e->getMessage(), true));
			Log::warning(' ------------------------------------ ');
		}
	}
}
