<?php

namespace App\Console;

use App\Console\Commands\ContactReinscriptionCmd;
use App\Console\Commands\ContactsNewNotAssignedCmd;
use App\Console\Commands\ContactsParaisoNewNotAssignedCmd;
use App\Console\Commands\ContactParaisoRegistrationCmd;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		//
		ContactReinscriptionCmd::class,
		ContactsNewNotAssignedCmd::class,
		ContactParaisoRegistrationCmd::class,
		ContactsParaisoNewNotAssignedCmd::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		// $schedule->command('inspire')->hourly();

		// $schedule->call(function () {
		//     // your schedule code
		//     Log::info('Cron job Working');
		// })
		//     ->description('check that cronjob is working')
		//     ->everyMinute();



		//Contactos Villahermosa
		$schedule->command('hb:contact_reinscription')->everyMinute();
		$schedule->command('hb:contacts_new_not_assigned')->everyFifteenMinutes();

		//Contactos Paraiso
		$schedule->command('hb:contact_paraiso_reinscription')->everyMinute();
		$schedule->command('hb:contacts_paraiso_new_not_assigned')->everyFifteenMinutes();

		// $schedule->job(new HBContactReinscriptionJob())->everyMinute();

		// queue:work --tries=3 --once
		// $schedule->command('queue:work --tries=3')
		//     ->cron('* * * * *')
		//     ->withoutOverlapping(5);

		// $schedule->command('queue:work --tries=3')->everyMinute()->withoutOverlapping(5);
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
