<?php

namespace App\Http\Controllers;

use App\Models\GuardiasParaiso;
use App\Imports\GuardiasImport;
use App\Models\HBOwners;
use App\Models\UserGuardiasParaiso;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class GuardiasParaisoController extends Controller
{

	public function dashboard(Request $request, $date = null)
	{

		$year = Carbon::now()->year;
		$month = Carbon::now()->month;

		if ($date) {
			$dateArr = explode('-', $date);

			$year = $dateArr[0];
			$month = $dateArr[1];
		}

		if ($month > 12 || $month < 1) {
			$month = Carbon::now()->month;
		}

		$query = UserGuardiasParaiso::query()

			->whereYear('user_guardias_paraiso.start_at', '=', $year)
			->whereMonth('user_guardias_paraiso.start_at', '=', $month)
			->leftJoin('users', 'users.id', '=', 'user_guardias_paraiso.user_id')
			->leftJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
			->whereNull('users.deleted_at')
			->select('hb_owners.*', 'hb_owners.id as hb_owners_id', 'user_guardias_paraiso.*')
			->select('*')
			->orderBy('user_guardias_paraiso.id', 'asc')
			->get();

		// $user = DB::table('user_guardias_paraiso')
		// ->whereMonth('user_guardias_paraiso.start_at', $month)
		// ->whereYear('user_guardias_paraiso.start_at', $year)
		// ->leftJoin('users', 'users.id', '=', 'user_guardias_paraiso.user_id')
		// ->leftJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
		// ->whereNull('users.deleted_at')
		// ->select('hb_owners.*', 'hb_owners.id as hb_owners_id', 'user_guardias_paraiso.*')
		// ->orderBy('user_guardias_paraiso.id', 'asc')
		// ->get();

		$output = [];
		foreach ($query as $entry) {
			$dateStr = explode(' ', $entry->start_at)[0];

			$output[$dateStr][] = [
				'name' => $entry->name,
				'start_at' => $entry->start_at,
				'end_at' => $entry->end_at,
				'created_at' => $entry->created_at,
			];
		}
		// echo $query;

		return Inertia::render('AsesoresParaiso/Guardias/Dashboard', ['elements' => $output]);
	}
	/**
	 * Agregar Guardias desde un archivo CSV
	 */
	public function storeFromFile(Request $request)
	{

		$guadiasSaved = [];
		$guadiasNotSaved = [];
		$guadiasExistentes = [];

		$rules = array(
			'file' => 'required',
		);

		// Excel::import(new GuardiasImport, request()->file('file'));

		try {

			// $headings = (new HeadingRowImport())->toArray(request()->file('file'));
			// dd($headings);

			$array = Excel::toArray(new GuardiasImport, request()->file('file'));

			// dd($array);
			// var_dump($array);
			// dump($array);
			// exit();

			$weekStart = false;
			$guardiasStart = false;

			$weekDatesArr = [];

			$proyectsStart = false;
			$proyectsPerWeek = [];

			$guardsPerWeek = [];

			$guardsArr = [];

			$asesor = null;
			$i = 0;
			$j = 0;

			foreach ($array[0] as $rowNum => $row) {

				foreach ($row as $celdaNum => $celda) {

					// revisamos si la celda es una fecha
					if (DateTime::createFromFormat('m/d/Y', $celda) !== false) { // && (!$proyectsStart && !$guardiasStart)

						if (!$weekStart && $celdaNum == 1) {
							$weekDatesArr = [];
						}

						$weekDatesArr[] = $celda;

						$weekStart = true;

						$proyectsStart = false;
						$guardiasStart = false;

						if (count($guardsPerWeek) > 0) {

							$guardsArr[] = $guardsPerWeek;
							// $guardsArr = array_merge($guardsArr, $guardsPerWeek);

							$guardsPerWeek = [];
						}
					}

					if ($guardiasStart) {

						if ($celdaNum == 0) {
							$asesor = $celda;

							$i = 0;
							$j = 0;
						} else {

							if ($celda != '') {
								$guardsPerWeek[$asesor][$weekDatesArr[$i]] = $celda; // . ' ' . $celdaNum;
							}

							if ($j++ >= count($proyectsPerWeek)) {
								$j = 0;
								$i++;
							}
						}
					}

					if (strtolower($celda) == 'lugar') {
						$proyectsStart = true;

						$weekStart = false;
						$guardiasStart = false;
					}

					if ($proyectsStart) {

						if ($celdaNum + 1 == count($row)) {
							$proyectsStart = false;

							$guardiasStart = true;
						}

						if (!in_array($celda, $proyectsPerWeek) && $celda != '' && $celdaNum > 0) {
							$proyectsPerWeek[] = $celda;
						}
					}
				} // column celds end

				// TODO: QUitar, es solo para pruebas
				// if ($rowNum == 56) {
				//     // dd($weekDatesArr, $proyectsPerWeek, $guardsPerWeek, $guardsArr);
				//     dd($guardsPerWeek, $guardsArr);
				//     // dump('x vueltas');
				//     exit();
				// }

			} // row end

			// Si es la ultima linea, agregamos las guardias por semana () al arreglo general
			if ($rowNum + 1 == count($array[0])) {
				$guardsArr[] = $guardsPerWeek;
			}

			// exit();

			// dd($proyectsPerWeek);
			// dd($weekDatesArr, $proyectsPerWeek, $guardsPerWeek, $guardsArr);
			// dd($weekDatesArr, $guardsArr);
			// dd($guardsArr);

		} catch (\Exception $e) {
			dd($e->getMessage());
		}

		if ($guardsArr) {

			foreach ($guardsArr as $weekGuards) {

				foreach ($weekGuards as $asesor => $userGuards) {

					$asesorDb = HBOwners::where('email', $asesor)->first();

					if (!$asesorDb) {
						$guadiasNotSaved[] = [
							'asesor' => $asesor,
							'guardias' => $userGuards,
						];
					} else {

						$user_id = $asesorDb->user_id;

						foreach ($userGuards as $date => $hours) {

							$date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');

							try {
								$tmpArr = explode('-', $hours);

								$start_at = $date . ' ' . $tmpArr[0];
								$end_at = $date . ' ' . $tmpArr[1];

								$start_at = Carbon::createFromDate($start_at)->format('Y-m-d H:i:s');
								$end_at = Carbon::createFromDate($end_at)->format('Y-m-d H:i:s');
							} catch (Exception $e) {
								Log::warning('storeFromFile - userGuard:  ' . $e->getMessage());
								echo $e->getMessage();
							}

							$userGuardExist = UserGuardiasParaiso::where('user_id', $user_id)
								->where('start_at', $start_at)
								->where('end_at', $end_at)
								->first();

							if ($userGuardExist) {

								$guadiasExistentes[] = [
									'asesor' => $asesor,
									'guardia' => [
										'user_id' => $user_id,
										'start_at' => $start_at,
										'end_at' => $end_at,
									],
									'existente' => $userGuardExist->toArray(),
								];
							} else {

								$userGuardias = new UserGuardiasParaiso();
								$userGuardias->user_id = $user_id;

								$userGuardias->start_at = $start_at;
								$userGuardias->end_at = $end_at;

								$userGuardias->save();

								$guadiasSaved[] = $userGuardias->toArray();
							}
						}
					}
				}
			}
		}

		$responseData = [
			'guadiasSaved' => $guadiasSaved,
			'guadiasNotSaved' => $guadiasNotSaved,
			'guadiasExistentes' => $guadiasExistentes,
		];

		return back()->with('success', 'Realizado');

		// return redirect()->route('guardias.dashboard');

	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		//
		$request->validate([
			'from' => 'required_with:to|date|date_format:Y-m-d\TH:i:s',
			'to' => 'required_with:from|after:from|date|date_format:Y-m-d\TH:i:s',
		]);

		// dd($request->all());

		// $validator = Validator::make($request->all(),
		//     [
		//         'from' => 'required_with:to|date|date_format:Y-m-d\TH:i:s',
		//         'to' => 'required_with:from|after:from|date|date_format:Y-m-d\TH:i:s',
		//     ]
		// );

		// $errors = $validator->errors();
		// if ($errors) {
		//     dd($errors);
		// }

		$nowDate = Carbon::now();
		$nowDateStr = $nowDate->format('Y-m-d');

		$from = $request->input('from') ?? $nowDateStr . ' 00:00:00';
		$to = $request->input('to') ?? $nowDateStr . ' 23:59:59';

		$responseData = $this->indexHelper($from, $to);

		return Inertia::render('AsesoresParaiso/Guardias/Index', $responseData);
	}

	public function index_api(Request $request)
	{

		$request->validate([
			'from' => 'required_with:to|date|date_format:Y-m-d\TH:i:s',
			'to' => 'required_with:from|after:from|date|date_format:Y-m-d\TH:i:s',
		]);

		$nowDate = Carbon::now();
		$nowDateStr = $nowDate->format('Y-m-d');

		$from = $request->input('from') ?? $nowDateStr . ' 00:00:00';
		$to = $request->input('to') ?? $nowDateStr . ' 23:59:59';

		$responseData = $this->indexHelper($from, $to);

		// if (!$request->ajax()) {
		//     return Inertia::render('Asesores/Guardias/Dashboard', $responseData);
		// } else {
		return response()->json($responseData);
		// }
	}

	private function indexHelper($from, $to)
	{
		$nowDate = Carbon::now();
		$asesoresDisponiblesHoy = asesores_disponibles($from, $to, config('hubspot.asesores_paraiso_teamname'));
		$asesoresDisponiblesEnEsteMomento = asesores_disponibles_en_este_momento($from, $to, config('hubspot.asesores_paraiso_teamname'));

		$responseData = [
			'server_datetime' => $nowDate->format('Y-m-d\TH:i:s'),
			'asesores_disponibles_hoy' => $asesoresDisponiblesHoy,
			'asesores_disponibles_en_este_momento' => $asesoresDisponiblesEnEsteMomento,
		];

		return $responseData;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->all();

		$data['start_at'] = Carbon::createFromDate($data['start_at'])->format('Y-m-d H:i:s');
		$data['end_at'] = Carbon::createFromDate($data['end_at'])->format('Y-m-d H:i:s');

		$guadiasSaved = [];
		foreach ($data['user_ids'] as $user_id) {

			try {
				$userGuardias = new UserGuardiasParaiso();
				$userGuardias->user_id = $user_id;

				$userGuardias->start_at = $data['start_at'];
				$userGuardias->end_at = $data['end_at'];

				$userGuardias->save();

				$guadiasSaved[$user_id] = $userGuardias->toArray();
			} catch (Exception $e) {
				$guadiasSaved[$user_id] = 'Invalid ID';
			}
		}

		$responseData = [
			'done' => 'ok',
			'guadias_saved' => $guadiasSaved,
		];

		session()->flash('flash.banner', 'Guardias agregadas!');
		session()->flash('flash.bannerStyle', 'success');

		return response()->json($responseData);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\GuardiasParaiso  $guardiasParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function show(GuardiasParaiso $guardiasParaiso)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\GuardiasParaiso  $guardiasParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function edit(GuardiasParaiso $guardiasParaiso)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\GuardiasParaiso  $guardiasParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, GuardiasParaiso $guardiasParaiso)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\GuardiasParaiso  $guardiasParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $userGuardia)
	{
		//Se localiza la guardía y se elimina
		$UgP = UserGuardiasParaiso::find($userGuardia);
		$UgP->delete();
		$responseData = ['Done'];

		return response()->json($responseData);
	}
}
