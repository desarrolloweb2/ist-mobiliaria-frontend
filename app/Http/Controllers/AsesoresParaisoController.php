<?php

namespace App\Http\Controllers;

use App\Models\AsesoresParaiso;
use App\Models\HBOwners;
use App\Models\User;
use App\resources\HBResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;

class AsesoresParaisoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{


		$result = ['data' => []];

		$usersHBOwners = User::query()
			->rightJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
			->select('hb_owners.*')
			->Where('hb_owners.meta', 'LIKE', '%' . config('hubspot.asesores_paraiso_teamname') . '%')
			->paginate(10);

		return Inertia::render('AsesoresParaiso/List', [
			'elements' => $usersHBOwners,
		]);
	}
	public function index_api(Request $request)
	{

		$query = HBOwners::query()->where('hb_owners.meta', 'LIKE', '%' . config('hubspot.asesores_paraiso_teamname') . '%')->orderBy('user_id')->get();

		$responseData = $query;

		return response()->json($responseData);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\AsesoresParaiso  $asesoresParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function show(AsesoresParaiso $asesoresParaiso)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\AsesoresParaiso  $asesoresParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function edit(AsesoresParaiso $asesoresParaiso)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\AsesoresParaiso  $asesoresParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, AsesoresParaiso $asesoresParaiso)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\AsesoresParaiso  $asesoresParaiso
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(HBOwners $HBOwners)
	{
		$name = $HBOwners->user->name;

		$HBOwners->user->delete();
		$HBOwners->delete();

		Session::flash('success', 'Realizado');

		return redirect()->back()->with('message', 'Registro eliminado: ' . $name);
	}

	/**
	 * Actualiza los asesores, con la información que este en HB
	 */
	public function HBUpdateOwnersAPI()
	{

		$responseData = ['done' => 'success'];

		$HBResources = HBResources::UpdateHBOwners(config('hubspot.asesores_paraiso_teamname'));

		if (!empty($HBResources['errors'])) {
			$responseData['done'] = 'error';
		} else {
			$responseData['data'] = $HBResources['data'];
		}

		//return response()->json($responseData);
		//return back()->with('responseData', $responseData);
		return back()->with('success', 'Realizado');
	}
}
