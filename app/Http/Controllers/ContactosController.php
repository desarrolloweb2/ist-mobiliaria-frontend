<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ContactosController extends Controller
{

    //

    public function index(Request $request)
    {

        $result = ['data' => []];

        $usersHBOwners = User::query()
        // ->rightJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
        // ->select('hb_owners.*')
        // ->get()
            ->paginate(10)
        ;

        return Inertia::render('Contactos/List', [
            'elements' => $usersHBOwners,
        ]);
    }

}