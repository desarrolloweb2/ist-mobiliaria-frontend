<?php

namespace App\Http\Controllers;

use App\Models\ContactAssignment;
use App\Models\ContactAssignmentHistory;
use App\Models\ContactToReinscription;
use App\resources\HBResources;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class APIEndpointsForHB extends Controller
{

    /**
     * Asignación de un asesor a un contacto.
     * Se selecciona un asesor disponible en el momento que ingrese el contacto
     */
    public function leadRotation(Request $request)
    {

        Log::info(' ======================================= ');
        Log::info(' /api/lead-rotation');
        Log::info(' ======================================= ');

        $data = $request->json()->all();

        if (isset($data['objectType']) && $data['objectType'] == 'CONTACT') {
            if (isset($data['objectId'])) {

                $contactId = (Int) $data['objectId'];

                $hbContact = null;

                $hbResourcesRes = HBResources::createHBContactIfNotExists($contactId);
                if (empty($hbResourcesRes['errors'])) {
                    $hbContact = $hbResourcesRes['data'];
                }

                /**
                 * asignar propietario al lead o asesor al lead
                 * {{hubSpotBaseUrl}}/crm/v3/objects/contacts/:contactid?hapikey={{key cta d pruebas}}
                 *
                 */

                $hubSpotBaseUrl = config('hubspot.base_url');
                $hapikey = config('hubspot.hapy_key'); //'6a4d3deb-96d4-4124-ae79-b468f3698314'; // key cta d pruebas
                $resourceUrl = '/crm/v3/objects/contacts/' . $contactId;

                //Equipo de Rotación
                $setTeam = $data['team'];

                // buscamos un asesor que este disponible en este momento
                $asesorDisponible = $this->asesorDisponible($contactId,$setTeam);

                $hubspotOwnerId = null;

                if ($asesorDisponible) {

                    $hubspotOwnerId = $asesorDisponible['hb_id'];

                    try {

                        $response = Http::patch($hubSpotBaseUrl . $resourceUrl . '?hapikey=' . $hapikey, [
                            "properties" => [
                                "hubspot_owner_id" => (Int) $hubspotOwnerId,
                            ],
                        ]);

                        if ($response->status() !== 200) {
                            Log::warning(' ------------------------------------ ');
                            Log::warning(' error: hb call');
                            Log::warning(print_r($response->status(), true));
                            Log::warning(print_r($response->body(), true));
                            Log::warning(' ------------------------------------ ');

                            if (App::environment() != 'production') {
                                // dd($response->request())

                                return response($response->body(), $response->status());
                            }
                            return response('', 500);

                        } else {

                            Log::info(' ------------------------------------ ');
                            // Log::info(print_r($response->body(), true));
                            Log::info(' contacto(lead) ' . $contactId . ' (' . $hbContact['email'] . ') asignado a propietario ' . $hubspotOwnerId . ' (#' . $asesorDisponible['hb_owners_id'] . ' ) '); // . ' - ' . $asesorDisponible['user']['email']
                            Log::info(' ------------------------------------ ');

                            // Registrar que se le asigno al asesor, para que si no lo toma, al reinscribir, no lo tome en cuenta
                            $contactAssignment = new ContactAssignment();
                            $contactAssignment->hb_owner_id = $asesorDisponible['hb_owners_id'];
                            $contactAssignment->hb_contact_id = $contactId;
                            $contactAssignment->team = $setTeam;
                            $contactAssignment->save();

                            $contactAssignmentHistory = new ContactAssignmentHistory();
                            $contactAssignmentHistory->hb_owner_id = $asesorDisponible['hb_owners_id'];
                            $contactAssignmentHistory->hb_contact_id = $contactId;
                            $contactAssignmentHistory->team = $setTeam;
                            $contactAssignmentHistory->save();

                            return response()->json(['done' => 'success', 'owner_id' => $hubspotOwnerId, 'hb_owners_id' => $asesorDisponible['hb_owners_id']], 200, );
                        }

                    } catch (Exception $e) {
                        Log::warning(' ------------------------------------ ');
                        Log::warning(' error al asignar lead a propietario ');
                        Log::warning(print_r($e->getCode(), true));
                        Log::warning(print_r($e->getMessage(), true));
                        Log::warning(' ------------------------------------ ');
                    }
                }

            }
        }

        return response('', 403);

    }

    /**
     * reinscripción de contacto
     */
    public function wfReinscription(Request $request)
    {

        // Log::useDailyFiles(storage_path().'/logs/wf-reinscription.log');
        // Log::useFiles(storage_path().'/logs/wf-reinscription.log');

        Log::info(' ======================================= ');
        Log::info(' /api/wf-reinscription');
        Log::info(' ======================================= ');

        $data = $request->json()->all();

        Log::info(' ------------------------------------ ');
        // Log::info(print_r($data, true));
        Log::info('', $data);
        Log::info(' ------------------------------------ ');

        try {
            ContactToReinscription::updateOrCreate(
                ['hb_contact_id' => $data['objectId']],
              //  ['hb_contact_id' => $data['objectId']],
                ['team' => $data['team']]
            );

            Log::info(' ------------------------------------ ');
            Log::info(' Contacto ' . $data['objectId'] . ' guardado para reinscribir en workflow - wfReinscription');
            Log::info(' ------------------------------------ ');

            return response()->json(['done' => 'success'], 200);

        } catch (Exception $e) {
            Log::warning(' ------------------------------------ ');
            Log::warning(' Error ');
            Log::warning(print_r($e->getCode(), true));
            Log::warning(print_r($e->getMessage(), true));
            Log::warning(' ------------------------------------ ');
            echo $e->getMessage();
            echo $data['team'];
        }

        return response('', 403);

    }

    /**
     * No tomar en cuenta a los asesores a los que se les ha intengado asignar previamente, a menos que ya haya
     * pasado por todos, debe de volver a empezar
     */
    private function asesorDisponible($hb_contact_id,$team)
    {
        $asesorDisponible = null;

        $asesoresDisponiblesEnEsteMomento = asesores_disponibles_en_este_momento(null,null,$team);

        if (count($asesoresDisponiblesEnEsteMomento) < 1) {
            Log::warning(' ------------------------------------ ');
            Log::warning(' Sin asesores disponibles ');
            Log::warning(' ------------------------------------ ');
        } else {

            $asesoresDisponiblesEnEsteMomentoIDs = array_map(function ($el) {return $el->hb_owners_id;}, $asesoresDisponiblesEnEsteMomento);

            /**
             * Buscamos el ultimo asesor al que le fue asignado un contacto cualquier y
             * el asesor al que ESTE conctacto, fue asisgnado por ultima vez
             */

            $asesoresElegiblesIDs = $asesoresDisponiblesEnEsteMomentoIDs;
            $asesoresNoElegiblesIDs = [];

            // ultimo asesor al que se le asigno cualquier contacto
            //$ultimoAsesorConAsignacion = ContactAssignment::query()->whereDate('created_at', Carbon::today())->latest('created_at')->first();
            $ultimoAsesorConAsignacion = ContactAssignment::query()->where('team','=',$team)->whereDate('created_at', Carbon::today())->latest('created_at')->first();
            if ($ultimoAsesorConAsignacion) {
                $asesoresNoElegiblesIDs[] = $ultimoAsesorConAsignacion->hb_owner_id;
            }

            // ultimo asesor, asignado a este contacto (en caso de que sea reinscripción)
            $ultimoAsesorAsignadoAlContacto = ContactAssignment::query()->where('team','=',$team)->whereDate('created_at', Carbon::today())->where('hb_contact_id', $hb_contact_id)->latest('created_at')->first();
            if ($ultimoAsesorAsignadoAlContacto) {
                $asesoresNoElegiblesIDs[] = $ultimoAsesorAsignadoAlContacto->hb_owner_id;
            }

            $usuariosYaUsados = ContactAssignment::query()->where('team','=',$team)->whereDate('created_at', Carbon::today())->get();

            // si la cantidad de usuarios usados, es igual a la de disponibles, reiniciamos la tabla de contacos asignados
            if ($usuariosYaUsados->count() >= count($asesoresElegiblesIDs)) {
                ContactAssignment::query()->where('team','=',$team) ->whereDate('created_at', Carbon::today())->delete(); // ->whereIn('hb_owner_id', $asesoresElegiblesIDs)
            } else {
                foreach ($usuariosYaUsados as $el) {
                    $asesoresNoElegiblesIDs[] = $el->hb_owner_id;
                }
            }

            // -----

            // quitamos los asesores no elegibles, de los elegibles
            foreach ($asesoresNoElegiblesIDs as $hb_owner_id) {
                if (($key = array_search($hb_owner_id, $asesoresElegiblesIDs)) !== false) {
                    unset($asesoresElegiblesIDs[$key]);
                }
            }

            if (count($asesoresElegiblesIDs) > 0) {
                $asesorDisponibleID = array_shift($asesoresElegiblesIDs); // el siguiente disponible
            } else {
                $asesorDisponibleID = $asesoresDisponiblesEnEsteMomentoIDs[0];
            }

            // buscamos el ID del asesor disponible, en el Array de objetos de asesores disponibles
            $asesoresDisponibles = array_filter($asesoresDisponiblesEnEsteMomento, function ($el) use ($asesorDisponibleID) {
                if ($el->hb_owners_id == $asesorDisponibleID) {
                    return true;
                }
                return false;
            });

            // regresa un array, por lo que sacamos el elemento
            $asesorDisponible = array_pop($asesoresDisponibles)->toArray();

        }

        // dd($asesorDisponible);

        return $asesorDisponible;
    }

    //
    //

}