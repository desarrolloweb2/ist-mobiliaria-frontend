<?php

namespace App\Http\Controllers;

use App\Models\HBOwners;
use App\Models\User;
use App\resources\HBResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;

class AsesoresController extends Controller
{

	//
	public function index(Request $request)
	{

		$result = ['data' => []];

		$usersHBOwners = User::query()
			->rightJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
			->select('hb_owners.*')
			->Where('hb_owners.meta', 'LIKE', '%'.config('hubspot.asesores_teamname') .'%')

			// ->get()
			->paginate(10);

		return Inertia::render('Asesores/List', [
			'elements' => $usersHBOwners,
		]);
	}

	/**
	 *
	 */
	public function index_api(Request $request)
	{

		$query = HBOwners::query()->where('meta', 'LIKE', '%'. config('hubspot.asesores_teamname') .'%')->orderBy('user_id')->get();

		$responseData = $query;

		return response()->json($responseData);
	}

	//
	public function destroy(HBOwners $HBOwners)
	{
		$name = $HBOwners->user->name;

		$HBOwners->user->delete();
		$HBOwners->delete();

		Session::flash('success', 'Realizado');

		return redirect()->back()->with('message', 'Registro eliminado: ' . $name);
	}

	/**
	 * Actualiza los asesores, con la información que este en HB
	 */
	public function HBUpdateOwnersAPI()
	{

		$responseData = ['done' => 'success'];

		$HBResources = HBResources::UpdateHBOwners(config('hubspot.asesores_teamname'));

		if (!empty($HBResources['errors'])) {
			$responseData['done'] = 'error';
		} else {
			$responseData['data'] = $HBResources['data'];
		}

		// return response()->json($responseData);
		//return back()->with('responseData', $responseData);
		return back()->with('success', 'Realizado');
	}
}
