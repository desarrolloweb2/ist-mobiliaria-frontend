<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    // protected $rootView = 'app';
    protected $rootView = 'layouts.app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            //

             // Synchronously
             'appName' => config('app.name'),

             // Lazily
             'auth.user' => fn () => $request->user() ? $request->user()->only( 'name', 'email') : null,

             //
             'envl' => App::environment('local') ? true : false,
            //  'hb_hk' => config('hubspot.hapy_key'),

             // 'csrf_token' => csrf_token(), //

            //  'flash' => [
            //     'message' => fn () => $request->session()->get('message'),
            //     // 'success' => fn () => $request->session()->get('success'),
            //     // 'warning' => fn () => $request->session()->get('warning'),
            //     // 'errors' => fn () => $request->session()->get('errors'),
            //     'toast' => fn () => $request->session()->get('toast'),
            // ],
            'flash' => fn () => $request->session()->only(['success', 'error', 'message']),

            // 'popstate' => false
        ]);
    }
}