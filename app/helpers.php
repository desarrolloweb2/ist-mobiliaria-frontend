<?php

use App\Models\UserGuardias;
use App\Models\UserGuardiasParaiso;
use Carbon\Carbon;

if (!function_exists('current_user')) {
    function current_user()
    {
        return auth()->user();
    }
}

if (!function_exists('asesores_disponibles')) {
    function asesores_disponibles($from = null, $to = null,$team=null)
    {
        $nowDateStr = Carbon::now()->format('Y-m-d');

        $from = !is_null($from) ? $from : $nowDateStr . ' 00:00:00';
        $to = !is_null($to) ? $to : $nowDateStr . ' 23:59:59';

        $format = 'Y-m-d H:i:s';

        $pos = strpos($from, 'T');
        if ($pos !== false) {
            $format = 'Y-m-d\TH:i:s';
        }

        $from = Carbon::createFromFormat($format, $from);
        $to = Carbon::createFromFormat($format, $to);
        $t = config('hubspot.asesores_paraiso_teamname');

        if($team == $t){

            $query = UserGuardiasParaiso::query()
            // ->whereDate('start_at', '>=', $from)
            // ->whereDate('end_at', '<=', $to)
                ->where('start_at', '>=', $from)
                ->where('end_at', '<=', $to)
                ->leftJoin('users', 'users.id', '=', 'user_guardias_paraiso.user_id')
                ->leftJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
                ->whereNull('users.deleted_at')
                ->select('hb_owners.*', 'hb_owners.id as hb_owners_id', 'user_guardias_paraiso.*')
                ->orderBy('user_guardias_paraiso.id', 'asc')
            // ->get()
            ;

        }else{

            $query = UserGuardias::query()
            // ->whereDate('start_at', '>=', $from)
            // ->whereDate('end_at', '<=', $to)
                ->where('start_at', '>=', $from)
                ->where('end_at', '<=', $to)
                ->leftJoin('users', 'users.id', '=', 'user_guardias.user_id')
                ->leftJoin('hb_owners', 'hb_owners.user_id', '=', 'users.id')
                ->whereNull('users.deleted_at')
                ->select('hb_owners.*', 'hb_owners.id as hb_owners_id', 'user_guardias.*')
                ->orderBy('user_guardias.id', 'asc')
            // ->get()
            ;
        }
        $availablesToday = $query->get();

        /**
         * Next lines for DEBUG Purpose
         */
        // DB::enableQueryLog();

        // dd($query->toSql());
        // dump($query->toSql());

        // dd(DB::getQueryLog());

        // $querySql = str_replace(array('?'), array('\'%s\''), $query->toSql());
        // $querySql = vsprintf($querySql, $query->getBindings());
        // dump($querySql);
        // dump($query->get()->toArray());

        // ------------------

        return $availablesToday;
    }
}

if (!function_exists('asesores_disponibles_en_este_momento')) {
    function asesores_disponibles_en_este_momento($from = null, $to = null, $team=null)
    {
        $asesoresDisponiblesHoy = asesores_disponibles($from, $to,$team);

        //check if date between two dates
        $currentDate = date('Y-m-d H:i:s');
        $currentDate = date('Y-m-d H:i:s', strtotime($currentDate));

        $asesoresDisponiblesEnEsteMomento = [];
        foreach ($asesoresDisponiblesHoy as $row) {

            $startDate = date('Y-m-d H:i:s', strtotime($row->start_at));
            $endDate = date('Y-m-d H:i:s', strtotime($row->end_at));

            if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                $asesoresDisponiblesEnEsteMomento[] = $row;
            }
        }

        return $asesoresDisponiblesEnEsteMomento;
    }
}