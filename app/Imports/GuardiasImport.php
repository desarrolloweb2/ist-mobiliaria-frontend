<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter
    ::default('none');

class GuardiasImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // dump($row);

        // return new UserGuardias([
        //     'name' => $row['name'],
        //     'introduction' => $row['introduction'],
        //     'location' => $row['location'],
        //     'cost' => $row['cost'],
        // ]);
        return $row;
    }

}