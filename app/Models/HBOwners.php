<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HBOwners extends Model
{
    //use HasFactory;
    use SoftDeletes;

    protected $table = 'hb_owners';

    //
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}