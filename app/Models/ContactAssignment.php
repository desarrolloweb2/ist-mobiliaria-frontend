<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactAssignment extends Model
{
    use HasFactory;

    public function hb_owners()
    {
        return $this->belongsTo(HBOwners::class, 'hb_owner_id', 'id');
    }
}