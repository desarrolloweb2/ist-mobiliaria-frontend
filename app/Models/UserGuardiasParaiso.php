<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGuardiasParaiso extends Model
{
    use HasFactory;
    protected $table = 'user_guardias_paraiso';
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
