module.exports = {
    semi: false,
    tabWidth: 4,
    useTabs: false,
    printWidth: 120,
    endOfLine: 'auto',
    singleQuote: true,
    trailingComma: 'es5',
    bracketSpacing: true,
    arrowParens: 'always',
    // htmlWhitespaceSensitivity: 'css', // https://prettier.io/docs/en/options.html
}
