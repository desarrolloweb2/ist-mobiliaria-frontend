const mix = require('laravel-mix')

require('laravel-mix-purgecss')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications()

mix.js('resources/js/app.js', 'public/js/app2.js')
    .vue()
    .postCss('resources/css/app.css', 'public/css/app2.css', [
        // prettier-ignore
        require('postcss-import'),
        require('tailwindcss'),
    ])
    .webpackConfig(require('./webpack.config'))
    .purgeCss({
        enabled: mix.inProduction(),
        // whitelist: ["card4_1", "card4_2", "card4_3", "card4_4"]
    })

if (mix.inProduction()) {
    mix.version()
}

// mix.browserSync({
//     proxy: process.env.APP_URL,
//     proxy: 'mlc.ist.local:8008',
//     notify: false,
//     port: 3008,
// })
