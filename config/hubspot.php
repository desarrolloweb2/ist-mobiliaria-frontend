<?php

return [
    'base_url' => env('HB_BASE_URL', ''),
    'hapy_key' => env('HB_HAPIKEY', ''),
    'workflow_paraiso_id' => env('HB_WORKFLOW_VENTAS_PARAISO_ID', ''),
    'workflow_villa_hermosa_id' => env('HB_WORKFLOW_VILLA_HERMOSA_ID', ''),
    'asesores_teamname' => env('HB_ASESORES_VILLA_H_TEAM', ''),
    'asesores_paraiso_teamname' => env('HB_ASESORES_PARAISO_TEAM', ''),
];